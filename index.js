/**Bài tập 1: Nhập 3 số nguyên. Xuât 3 số theo thứ tự tăng dần.
 *
 */

document
    .getElementById("sap-so-thu-tu")
    .addEventListener("click", function () {
        var a = document.getElementById("txt-number1").value * 1;
        var b = document.getElementById("txt-number2").value * 1;
        var c = document.getElementById("txt-number3").value * 1;
        var KetQua = null;
        if (a > b && b > c) {
            console.log("a > b > c", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${a} > ${b} > ${c}</div>`;
        }
        else if (a > c && c > b) {
            console.log("a > c > b", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${a} > ${c} > ${b}</div>`;
        }
        else if (b > a && a > c) {
            console.log("b > a > c", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${b} > ${a} > ${c}</div>`;
        }
        else if (b > c && c > a) {
            console.log("b > c > a", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${b} > ${c} > ${a}</div>`;
        }
        else if (c > a && a > b) {
            console.log("c > a > b", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${c} > ${a} > ${b}</div>`;
        }
        else {
            console.log("c > b > a", { a }, { b }, { c });
            document.getElementById("thutu").innerHTML = `<div>Số Thứ Tự: ${c} > ${b} > ${a}</div>`;
        }
    })

/**Bài tập 2: Chào hỏi các thành viên trong gia đình.
 * 
 */

document
    .getElementById("guiloichao")
    .addEventListener("click", function () {
        var thanhVienValue = document.getElementById("txt-thanhvien").value;
        switch (thanhVienValue) {
            case "B": {
                console.log("xin chào bố", { thanhVienValue });
                document.getElementById("xinchao").innerHTML = `<div> xin chào bố !</div>`
            }; break;
            case "M": {
                console.log("xin chào mẹ", { thanhVienValue });
                document.getElementById("xinchao").innerHTML = `<div> xin chào mẹ !</div>`
            }; break;
            case "A": {
                console.log("xin chào anh trai", { thanhVienValue });
                document.getElementById("xinchao").innerHTML = `<div>xin chào anh !</div>`
            }; break;
            case "E": {
                console.log("xin chào em gái", { thanhVienValue });
                document.getElementById("xinchao").innerHTML = `<div> xin chào em gái !</div>`
            }; break;
        }
    })

/** Bài tập 3: Kiểm tra số chẵn lẻ
 * 
 */
function demSoChanLe() {
    var num1Value = document.getElementById("txt-num1").value * 1;
    var num2Value = document.getElementById("txt-num2").value * 1;
    var num3Value = document.getElementById("txt-num3").value * 1;
    var count = 0;
    if (num1Value % 2 == 0) {
        count++
    }
    if (num2Value % 2 == 0) {
        count++
    }
    if (num3Value % 2 == 0) {
        count++
    }
    document.getElementById('txt-chan-le').innerHTML = `<div> có ${count} số chẵn, có ${3 - count} số lẻ</div>`
}

/** Bài tập 4: Dự đoán tam giác
 * 
 */
function duDoanTamGiac() {
    var canh1Value = document.getElementById("txt-canh1").value * 1;
    var canh2Value = document.getElementById("txt-canh2").value * 1;
    var canh3Value = document.getElementById("txt-canh3").value * 1;
    if (canh1Value == canh2Value && canh2Value == canh3Value) {
        document.getElementById("tamgiac").innerHTML = `<div>tam giác đều</div>`;
    } else if ((canh1Value == canh2Value) || (canh1Value == canh3Value) || (canh3Value == canh2Value)) {
        document.getElementById("tamgiac").innerHTML = `<div>tam giác cân</div>`;
    } else if ((canh1Value * canh1Value == canh2Value * canh2Value + canh3Value * canh3Value) || (canh2Value * canh2Value == canh1Value * canh1Value + canh3Value * canh3Value) || (canh3Value * canh3Value == canh2Value * canh2Value + canh1Value * canh1Value)) {
        document.getElementById("tamgiac").innerHTML = `<div>tam giác vuông</div>`;
    }
    else {
        document.getElementById("tamgiac").innerHTML = `<div>tam giác khác</div>`;
    }

}

